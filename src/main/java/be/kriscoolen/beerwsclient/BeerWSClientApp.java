package be.kriscoolen.beerwsclient;


import be.kriscoolen.cursusenterprisespring.opdracht5.service.Beer;
import be.kriscoolen.cursusenterprisespring.opdracht5.service.BeerServiceEndPoint;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.jaxws.JaxWsPortProxyFactoryBean;
import net.java.dev.jaxb.array.IntArray;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static be.kriscoolen.util.ConsoleInputTool.*;


@SpringBootApplication
public class BeerWSClientApp {
    public static void main(String[] args) throws Exception{
        ConfigurableApplicationContext ctx = SpringApplication.run(BeerWSClientApp.class,args);
        BeerServiceEndPoint beerService = ctx.getBean("beerService",BeerServiceEndPoint.class);
        //placeOrdersWithSingleOrderItem(beerService);
        placeOrdersWithMultipleOrderItems(beerService);
    }
    @Bean
    public JaxWsPortProxyFactoryBean beerService() throws MalformedURLException{
        JaxWsPortProxyFactoryBean proxy = new JaxWsPortProxyFactoryBean();
        proxy.setServiceInterface(BeerServiceEndPoint.class);
        proxy.setWsdlDocumentUrl(new URL("http://localhost/BeerService?wsdl"));
        proxy.setNamespaceUri("http://service.opdracht5.cursusenterprisespring.kriscoolen.be/");
        proxy.setServiceName("BeerService");
        proxy.setPortName("BeerServiceEndPointPort");
        return proxy;
    }

    public static void placeOrdersWithSingleOrderItem(BeerServiceEndPoint beerService){
       // ConfigurableApplicationContext ctx = SpringApplication.run(BeerWSClientApp.class,args);
       // BeerServiceEndPoint beerService = ctx.getBean("beerService",BeerServiceEndPoint.class);
        boolean placeNewOrder = true;
        int orderNumber = 0;
        while(placeNewOrder){
            String orderName = askUserInputString("Enter order name",1);
            boolean isInvalidOrder = true;
            int beerId = 0;
            int number = -1;
            int stock = -1;
            do {
                do {
                    beerId = askUserInputInteger("Enter beer id", 1);
                    Beer beer = beerService.getBeerById(beerId);
                    if(beer!=null){
                        stock = beer.getStock();
                        break;
                    }
                    else{
                        System.out.println("Beer id does not exist in DB. Try again.");
                    }
                }while(true);
                //beerId is a valid beer id!
                number = askUserInputInteger("Enter number of beers", 0,stock);
                //number is a valid number of beers to order
                try{
                    orderNumber = beerService.orderBeer(orderName,beerId,number);
                    isInvalidOrder = false;
                }catch(RuntimeException rte){
                    //normaal gezien zouden we hier nu niet meer mogen geraken!
                    System.out.println(rte.getMessage());
                    // rte.printStackTrace();
                    System.out.println("Either the beer id is not valid or number of beers.");
                    askPressEnterToContinue();
                }
            }while(isInvalidOrder);
            System.out.println("Your order is succesfully registered. Your order number: " + orderNumber);
            placeNewOrder = askUserYesNoQuestion("Would you like to place another order?");
        }
    }

    public static void placeOrdersWithMultipleOrderItems(BeerServiceEndPoint beerService){
        boolean placeNewOrder = true;
        int orderNumber = 0;
        while(placeNewOrder){
            String orderName = askUserInputString("Enter order name",1);
            boolean isInvalidOrder = true;
            int beerId = 0;
            int number = -1;
            int stock = -1;
            do {

                List<IntArray> order = new ArrayList<>();
                boolean newOrderItem = true;
                int i = 0;
                while(newOrderItem){
                    i++;
                    do {
                        beerId = askUserInputInteger("Enter beer id of your "+ i+"th order-item", 1);
                        Beer beer = beerService.getBeerById(beerId);
                        if(beer!=null){
                            stock = beer.getStock();
                            break;
                        }
                        else{
                            System.out.println("Beer id does not exist in DB. Try again.");
                        }
                    }while(true);
                    //beerId is a valid beer id!
                    number = askUserInputInteger("Enter number of beers of your "+ i+"th order-item", 0,stock);
                    //number is a valid number of beers to order
                    //add to order array
                    IntArray orderItem = new IntArray();
                    orderItem.getItem().add(beerId);
                    orderItem.getItem().add(number);
                    order.add(orderItem);
                    newOrderItem = askUserYesNoQuestion("Would you like to add a new order item to your order?");
                }
                try{
                    orderNumber = beerService.orderBeers(orderName,order);
                    isInvalidOrder = false;
                }catch(RuntimeException rte){
                    //normaal gezien zouden we hier nu niet meer mogen geraken!
                    System.out.println(rte.getMessage());
                    // rte.printStackTrace();
                    System.out.println("Your order is not valid! Please try again.");
                    askPressEnterToContinue();
                }
            }while(isInvalidOrder);
            System.out.println("Your order is succesfully registered. Your order number: " + orderNumber);
            placeNewOrder = askUserYesNoQuestion("Would you like to place another order?");
        }

    }
}
