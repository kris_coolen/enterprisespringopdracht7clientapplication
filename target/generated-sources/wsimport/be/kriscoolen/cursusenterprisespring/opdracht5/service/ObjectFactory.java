
package be.kriscoolen.cursusenterprisespring.opdracht5.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the be.kriscoolen.cursusenterprisespring.opdracht5.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetBeerById_QNAME = new QName("http://service.opdracht5.cursusenterprisespring.kriscoolen.be/", "getBeerById");
    private final static QName _GetBeerByIdResponse_QNAME = new QName("http://service.opdracht5.cursusenterprisespring.kriscoolen.be/", "getBeerByIdResponse");
    private final static QName _OrderBeer_QNAME = new QName("http://service.opdracht5.cursusenterprisespring.kriscoolen.be/", "orderBeer");
    private final static QName _OrderBeerResponse_QNAME = new QName("http://service.opdracht5.cursusenterprisespring.kriscoolen.be/", "orderBeerResponse");
    private final static QName _OrderBeers_QNAME = new QName("http://service.opdracht5.cursusenterprisespring.kriscoolen.be/", "orderBeers");
    private final static QName _OrderBeersResponse_QNAME = new QName("http://service.opdracht5.cursusenterprisespring.kriscoolen.be/", "orderBeersResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: be.kriscoolen.cursusenterprisespring.opdracht5.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetBeerById }
     * 
     */
    public GetBeerById createGetBeerById() {
        return new GetBeerById();
    }

    /**
     * Create an instance of {@link GetBeerByIdResponse }
     * 
     */
    public GetBeerByIdResponse createGetBeerByIdResponse() {
        return new GetBeerByIdResponse();
    }

    /**
     * Create an instance of {@link OrderBeer }
     * 
     */
    public OrderBeer createOrderBeer() {
        return new OrderBeer();
    }

    /**
     * Create an instance of {@link OrderBeerResponse }
     * 
     */
    public OrderBeerResponse createOrderBeerResponse() {
        return new OrderBeerResponse();
    }

    /**
     * Create an instance of {@link OrderBeers }
     * 
     */
    public OrderBeers createOrderBeers() {
        return new OrderBeers();
    }

    /**
     * Create an instance of {@link OrderBeersResponse }
     * 
     */
    public OrderBeersResponse createOrderBeersResponse() {
        return new OrderBeersResponse();
    }

    /**
     * Create an instance of {@link Beer }
     * 
     */
    public Beer createBeer() {
        return new Beer();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBeerById }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetBeerById }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.opdracht5.cursusenterprisespring.kriscoolen.be/", name = "getBeerById")
    public JAXBElement<GetBeerById> createGetBeerById(GetBeerById value) {
        return new JAXBElement<GetBeerById>(_GetBeerById_QNAME, GetBeerById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBeerByIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetBeerByIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.opdracht5.cursusenterprisespring.kriscoolen.be/", name = "getBeerByIdResponse")
    public JAXBElement<GetBeerByIdResponse> createGetBeerByIdResponse(GetBeerByIdResponse value) {
        return new JAXBElement<GetBeerByIdResponse>(_GetBeerByIdResponse_QNAME, GetBeerByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderBeer }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link OrderBeer }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.opdracht5.cursusenterprisespring.kriscoolen.be/", name = "orderBeer")
    public JAXBElement<OrderBeer> createOrderBeer(OrderBeer value) {
        return new JAXBElement<OrderBeer>(_OrderBeer_QNAME, OrderBeer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderBeerResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link OrderBeerResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.opdracht5.cursusenterprisespring.kriscoolen.be/", name = "orderBeerResponse")
    public JAXBElement<OrderBeerResponse> createOrderBeerResponse(OrderBeerResponse value) {
        return new JAXBElement<OrderBeerResponse>(_OrderBeerResponse_QNAME, OrderBeerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderBeers }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link OrderBeers }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.opdracht5.cursusenterprisespring.kriscoolen.be/", name = "orderBeers")
    public JAXBElement<OrderBeers> createOrderBeers(OrderBeers value) {
        return new JAXBElement<OrderBeers>(_OrderBeers_QNAME, OrderBeers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderBeersResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link OrderBeersResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.opdracht5.cursusenterprisespring.kriscoolen.be/", name = "orderBeersResponse")
    public JAXBElement<OrderBeersResponse> createOrderBeersResponse(OrderBeersResponse value) {
        return new JAXBElement<OrderBeersResponse>(_OrderBeersResponse_QNAME, OrderBeersResponse.class, null, value);
    }

}
